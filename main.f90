PROGRAM main
  use mpi
  USE amsta01maillage
  USE amsta01sparse
  USE amsta01probleme
  IMPLICIT NONE
  TYPE(maillage) :: mail,new_mail
  TYPE(probleme) :: pb,new_pb
  TYPE(matsparse) :: Kt, Mt, Test,MJac,NJac,J
  REAL(kind=8) :: erreur
  REAL(kind=8), DIMENSION(:), POINTER :: residu
  integer, dimension(:), pointer :: inter,sol
  integer :: ierr,nbTask,myRank,req,status,i
 
  call MPI_Init(ierr)

  call MPI_COMM_SIZE(MPI_COMM_WORLD, nbTask, ierr)
  call MPI_COMM_RANK(MPI_COMM_WORLD, myRank, ierr)


  ! lecture du maillage
  mail = loadFromMshFile("./testpart.msh")
  ! construction des donnees sur les triangles
  CALL getTriangles(mail)

  ! creation du probleme

!  if (myRank==0) then
     CALL loadFromMesh(pb,mail)
 ! end if
   ! assemblage des matrices elements finis
!  CALL assemblage(pb)
  ! pseudo-elimination des conditions essentielles
!  CALL pelim(new_pb,new_mail%refNodes(1))


  ! creation du probleme

  ! Pour chaque partition
     
     if (myRank==0) then
        allocate(pb%u_poub1(mail%nbNodes))
        allocate(pb%u_poub2(mail%nbNodes))
        call mpi_recv(pb%u_poub1,pb%mesh%nbNodes,MPI_REAL8,1,1,MPI_COMM_WORLD,status,ierr)
        call mpi_recv(pb%u_poub2,mail%nbNodes,MPI_REAL8,2,1,MPI_COMM_WORLD,status,ierr)
        pb%u=pb%u_poub1+pb%u_poub2
        call assembleInter(mail,inter)
        print *, 'pts interface', size(inter)

        do i=1,size(inter)
           pb%u(inter(i))=pb%u(inter(i))/2.d0
        end do

        CALL saveToVtu(pb%mesh,pb%u,pb%uexa)
        
     else
        
        call sous_maillage(new_mail,mail,myRank)
        CALL loadFromMesh(new_pb,new_mail)
        ! assemblage des matrices elements finis
        CALL assemblage(new_pb)
        ! pseudo-elimination des conditions essentielles
        CALL pelim(new_pb,new_mail%refNodes(1))
        
        ! calcul du residu theorique
        ALLOCATE(residu(new_mail%nbNodes))
        residu=new_pb%felim-new_pb%p_Kelim*new_pb%uexa
        erreur=dsqrt(DOT_PRODUCT(residu,residu))
        PRINT *,"proc",myRank, "residu theorique=", erreur
  
        ! call assembleInter(mail,inter)
  
  
        ! Pour chaque partition
  
  
        print*, "nouveau maillage",new_mail%nbNodes
  
        ! On teste nos nouvelles fonctions
        !CALL test_amsta01sparse
        ! resolution du systeme lineaire
        CALL solveLU(new_pb)
        !CALL Jacobi(pb)
        !CALL Gausseidel(pb)
        
        !call backHome(pb,mail,new_pb)
        

        !   call mpi_send(new_pb%u,1,mpi_real8,0,1,mpi_comm_world,ierr)
        ! if (myRank == 0) then
        !   allocate(pb%u_poub1(mail%nbNodes))
        !  allocate(pb%u_poub2(mail%nbNodes))
        ! call mpi_recv(pb%u_poub1,pb%mesh%nbNodes,MPI_REAL8,1,1,MPI_COMM_WORLD,status,ierr)
        !call mpi_recv(pb%u_poub2,mail%nbNodes,MPI_REAL8,2,1,MPI_COMM_WORLD,status,ierr)
        ! else

        call backHome(pb,new_pb)
        call mpi_send(pb%u,mail%nbNodes,mpi_real8,0,1,mpi_comm_world,ierr)
        !end if

        ! calcul du residu
        residu=new_pb%felim-new_pb%p_Kelim*new_pb%u
        erreur=dsqrt(DOT_PRODUCT(residu,residu))
        PRINT *,"proc",myRank, "residu=", erreur
        
        ! calcul de l'erreur L2
        erreur=dsqrt(DOT_PRODUCT(new_pb%uexa-new_pb%u,new_pb%uexa-new_pb%u))
        PRINT *,"proc ",myRank, "||u-uexa||_2=", erreur
        
     end if

     ! sauvegarde de la solution et de la solution theorique
     !CALL saveToVtu(pb%mesh,pb%u,pb%uexa)

     call MPI_Finalize(ierr)



     !CALL saveToVtu(pb%mesh,pb%u,pb%uexa)

END PROGRAM main
