! ----------------------------------------------------------------
! module d'algebre lineaire sur les matrices sparse
!
! On stocke uniquement les indice de ligne et de colonne,
! ainsi que la valeur d'un coefficient non nul d'une matrice (i, j, val)
! dans 3 tableaux separes
! On stocke egalement les dimensions de la matrice, ainsi qu'un
! indicateur d'allocation des tableaux
!
! Les coefficients ne sont pas necessairement ranges dans l'ordre
! Auteur : N. Kielbasiewicz
! -----------------------------------------------------------------
MODULE amsta01sparse
  IMPLICIT NONE
  TYPE matsparse
    INTEGER :: n, m                            ! nombre de lignes et nombre de colonnes de la matrice
    INTEGER, DIMENSION(:), POINTER :: i, j     ! tableaux des indices de ligne et de colonne des coefficients non nuls
    REAL(kind=8), DIMENSION(:), POINTER :: val ! tableau des valeurs des coefficients
    LOGICAL :: isAllocated                     ! true si les 3 tableaux sont alloues (a la meme taille)
  END TYPE
  INTERFACE find
    MODULE PROCEDURE findpos, findind
  END INTERFACE
  INTERFACE OPERATOR (+)
    MODULE PROCEDURE spadd
  END INTERFACE
  INTERFACE OPERATOR (-)
      MODULE PROCEDURE spminus
    END INTERFACE
    INTERFACE ASSIGNMENT (=)
    MODULE PROCEDURE spaffect, spcopy
  END INTERFACE
  INTERFACE OPERATOR (*)
    MODULE PROCEDURE spscalmat,spmatscal,spmatvec,spmatmat
  END INTERFACE
  CONTAINS
    ! initialisation d'une matrice
    !     a : matrice
    !     n : nombre de lignes
    !     m : nombre de colonnes
    !     nnz : nombre de coefficients non nuls (optionel)
    !     si nnz est precise, les tableaux sont alloues
    SUBROUTINE sparse(a, n, m, nnz)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(out) :: a
      INTEGER, INTENT(in) :: n, m
      INTEGER, INTENT(in), OPTIONAL :: nnz
      a%n=n
      a%m=m
      a%isAllocated=.FALSE.
      IF (PRESENT(nnz)) THEN
        ALLOCATE(a%i(nnz),a%j(nnz),a%val(nnz))
        a%isAllocated=.TRUE.
      END IF
    END SUBROUTINE sparse
    ! allocation d'une matrice non allouee
    SUBROUTINE msallocate(a,nnz)
      TYPE(matsparse), INTENT(inout) :: a
      INTEGER, INTENT(in) :: nnz
      IF (a%isAllocated.EQV..TRUE.) THEN
        STOP "ERROR in msallocate: matsparse already allocated"
      END IF
      ALLOCATE(a%i(nnz),a%j(nnz),a%val(nnz))
      a%isAllocated=.TRUE.
    END SUBROUTINE msallocate
    ! desaloccation d'une matrice
    SUBROUTINE msdeallocate(a)
      TYPE(matsparse), INTENT(inout) :: a

      IF (a%isAllocated.EQV..FALSE.) THEN
        STOP "ERROR in msdeallocate: matsparse already deallocated"
      END IF
      DEALLOCATE(a%i,a%j,a%val)
      a%isAllocated=.FALSE.
    END SUBROUTINE msdeallocate
    ! reallocation d'une matrice
    SUBROUTINE msreallocate(a,nnz)
      TYPE(matsparse), INTENT(inout) :: a
      INTEGER, INTENT(in) :: nnz
      IF (a%isAllocated.EQV..FALSE.) THEN
        STOP "ERROR in msreallocate: matsparse already deallocated"
      END IF
      DEALLOCATE(a%i,a%j,a%val)
      ALLOCATE(a%i(nnz),a%j(nnz),a%val(nnz))
      a%isAllocated=.TRUE.
    END SUBROUTINE msreallocate
    ! fonction de modification/insertion d'un coefficient
    !     si la matrice est vide, elle contient cet unique coefficient
    !     sinon, soit on ecrase le coefficient s'il existe, soit on le cree
    SUBROUTINE setcoeff(a, i, j, val)
      IMPLICIT NONE
      INTEGER, INTENT(in) :: i, j
      TYPE(matsparse), INTENT(inout) :: a
      REAL(kind=8), INTENT(in) :: val
      INTEGER, DIMENSION(:), POINTER :: itmp
      INTEGER, DIMENSION(:), POINTER :: jtmp
      REAL(kind=8), DIMENSION(:), POINTER :: vtmp
      INTEGER :: nnz, ind
      LOGICAL :: isEmpty

      IF (i>a%n) THEN
        STOP "ERROR in setcoeff: row index out of bounds" 
      END IF
      IF (j>a%m) THEN
        STOP "ERROR in setcoeff: column index out of bounds"
      END IF

      isEmpty=.FALSE.
      IF (a%isAllocated.EQV..FALSE.) THEN
        CALL msallocate(a,1)
        isEmpty=.TRUE.
      END IF

      IF (SIZE(a%i) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays i and j in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%i) /= SIZE(a%val)) THEN
        STOP "ERROR in setcoeff: arrays i and val in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%val) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays j and val in matsparse must have the same size (did you use msallocate ?)"
      END IF

      IF (isEmpty.EQV..TRUE.) THEN
        a%i(1)=i
        a%j(1)=j
        a%val(1)=val
      ELSE
        ind=findpos(a, i, j)
        IF (ind /= 0) THEN ! coeff already exists
          a%val(ind)=val
        ELSE ! coeff does not exists, we add it to the storage
          nnz=SIZE(a%i)
          ALLOCATE(itmp(nnz), jtmp(nnz), vtmp(nnz))
          itmp=a%i
          jtmp=a%j
          vtmp=a%val
          DEALLOCATE(a%i,a%j,a%val)
          ALLOCATE(a%i(nnz+1),a%j(nnz+1),a%val(nnz+1))
          a%i(1:nnz)=itmp
          a%j(1:nnz)=jtmp
          a%val(1:nnz)=vtmp
          a%i(nnz+1)=i
          a%j(nnz+1)=j
          a%val(nnz+1)=val
          DEALLOCATE(itmp,jtmp,vtmp)
        END IF
      END IF
    END SUBROUTINE setcoeff
    ! fonction d'ajout d'une valeur a un coefficient
    !     si la matrice est vide, elle contient cet unique coefficient
    SUBROUTINE addtocoeff(a, i, j, val)
      IMPLICIT NONE
      INTEGER, INTENT(in) :: i, j
      TYPE(matsparse), INTENT(inout) :: a
      REAL(kind=8), INTENT(in) :: val
      INTEGER, DIMENSION(:), POINTER :: itmp
      INTEGER, DIMENSION(:), POINTER :: jtmp
      REAL(kind=8), DIMENSION(:), POINTER :: vtmp
      INTEGER :: nnz, ind
      LOGICAL :: isEmpty

      IF (i>a%n) THEN
        STOP "ERROR in setcoeff: row index out of bounds" 
      END IF
      IF (j>a%m) THEN
        STOP "ERROR in setcoeff: column index out of bounds"
      END IF

      isEmpty=.FALSE.
      IF (a%isAllocated.EQV..FALSE.) THEN
        CALL msallocate(a,1)
        isEmpty=.TRUE.
      END IF

      IF (SIZE(a%i) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays i and j in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%i) /= SIZE(a%val)) THEN
        STOP "ERROR in setcoeff: arrays i and val in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%val) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays j and val in matsparse must have the same size (did you use msallocate ?)"
      END IF

      IF (isEmpty.EQV..TRUE.) THEN
        a%i(1)=i
        a%j(1)=j
        a%val(1)=val
      ELSE
        ind=findpos(a, i, j)
        IF (ind /= 0) THEN ! coeff already exists
          a%val(ind)=a%val(ind)+val
        ELSE ! coeff does not exists, we add it to the storage
          nnz=SIZE(a%i)
          ALLOCATE(itmp(nnz), jtmp(nnz), vtmp(nnz))
          itmp=a%i
          jtmp=a%j
          vtmp=a%val
          DEALLOCATE(a%i,a%j,a%val)
          ALLOCATE(a%i(nnz+1),a%j(nnz+1),a%val(nnz+1))
          a%i(1:nnz)=itmp
          a%j(1:nnz)=jtmp
          a%val(1:nnz)=vtmp
          a%i(nnz+1)=i
          a%j(nnz+1)=j
          a%val(nnz+1)=val
          DEALLOCATE(itmp,jtmp,vtmp)
        END IF
      END IF
    END SUBROUTINE addtocoeff
    ! fonction dde multiplication d'un coefficient par une valeur
    SUBROUTINE multcoeff(a, i, j, val)
      IMPLICIT NONE
      INTEGER, INTENT(in) :: i, j
      TYPE(matsparse), INTENT(inout) :: a
      REAL(kind=8), INTENT(in) :: val
      INTEGER, DIMENSION(:), POINTER :: itmp
      INTEGER, DIMENSION(:), POINTER :: jtmp
      REAL(kind=8), DIMENSION(:), POINTER :: vtmp
      INTEGER :: nnz, ind
      LOGICAL :: isEmpty

      IF (i>a%n) THEN
        STOP "ERROR in setcoeff: row index out of bounds" 
      END IF
      IF (j>a%m) THEN
        STOP "ERROR in setcoeff: column index out of bounds"
      END IF

      isEmpty=.FALSE.
      IF (a%isAllocated.EQV..FALSE.) THEN
        CALL msallocate(a,1)
        isEmpty=.TRUE.
      END IF

      IF (SIZE(a%i) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays i and j in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%i) /= SIZE(a%val)) THEN
        STOP "ERROR in setcoeff: arrays i and val in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%val) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays j and val in matsparse must have the same size (did you use msallocate ?)"
      END IF

      IF (isEmpty.EQV..TRUE.) THEN
        a%i(1)=i
        a%j(1)=j
        a%val(1)=val
      ELSE
        ind=findpos(a, i, j)
        IF (ind /= 0) THEN ! coeff already exists
          a%val(ind)=a%val(ind)*val
        ELSE ! coeff does not exists, we add it to the storage
          nnz=SIZE(a%i)
          ALLOCATE(itmp(nnz), jtmp(nnz), vtmp(nnz))
          itmp=a%i
          jtmp=a%j
          vtmp=a%val
          DEALLOCATE(a%i,a%j,a%val)
          ALLOCATE(a%i(nnz+1),a%j(nnz+1),a%val(nnz+1))
          a%i(1:nnz)=itmp
          a%j(1:nnz)=jtmp
          a%val(1:nnz)=vtmp
          a%i(nnz+1)=i
          a%j(nnz+1)=j
          a%val(nnz+1)=val
          DEALLOCATE(itmp,jtmp,vtmp)
        END IF
      END IF
    END SUBROUTINE multcoeff
    ! fonction de suppresion d'un coefficient
    SUBROUTINE delcoeff(a, i, j)
      IMPLICIT NONE
      INTEGER, INTENT(in) :: i, j
      TYPE(matsparse), INTENT(inout) :: a
      INTEGER, DIMENSION(:), POINTER :: itmp
      INTEGER, DIMENSION(:), POINTER :: jtmp
      REAL(kind=8), DIMENSION(:), POINTER :: vtmp
      INTEGER :: nnz, ind

      IF (i>a%n) THEN
        STOP "ERROR in setcoeff: row index out of bounds" 
      END IF
      IF (j>a%m) THEN
        STOP "ERROR in setcoeff: column index out of bounds"
      END IF

      IF (a%isAllocated.EQV..FALSE.) THEN
        RETURN 
      END IF

      IF (SIZE(a%i) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays i and j in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%i) /= SIZE(a%val)) THEN
        STOP "ERROR in setcoeff: arrays i and val in matsparse must have the same size (did you use msallocate ?)"
      END IF
      IF (SIZE(a%val) /= SIZE(a%j)) THEN
        STOP "ERROR in setcoeff: arrays j and val in matsparse must have the same size (did you use msallocate ?)"
      END IF

      ind=findpos(a, i, j)
      IF (ind /= 0) THEN ! coeff exists

        nnz=SIZE(a%i)
        ALLOCATE(itmp(nnz-1), jtmp(nnz-1), vtmp(nnz-1))
        itmp(1:ind-1)=a%i(1:ind-1)
        itmp(ind:nnz-1)=a%i(ind+1:nnz)
        jtmp(1:ind-1)=a%j(1:ind-1)
        jtmp(ind:nnz-1)=a%j(ind+1:nnz)
        vtmp(1:ind-1)=a%val(1:ind-1)
        vtmp(ind:nnz-1)=a%val(ind+1:nnz)
        DEALLOCATE(a%i,a%j,a%val)
        ALLOCATE(a%i(nnz-1),a%j(nnz-1),a%val(nnz-1))
        a%i=itmp
        a%j=jtmp
        a%val=vtmp
        DEALLOCATE(itmp,jtmp,vtmp)
      END IF
    END SUBROUTINE delcoeff
    ! fonction de recuperation d'un coefficient
    FUNCTION coeff(a, i, j) RESULT(val)
      TYPE(matsparse), INTENT(in) :: a
      INTEGER, INTENT(in) :: i, j
      REAL(kind=8) :: val
      INTEGER :: ind

      ind=findpos(a, i, j)
      IF (ind /= 0) THEN
        val=a%val(ind)
      ELSE
        val=0.d0
      END IF
    END FUNCTION coeff
    ! fonction de definition de la taille de la matrice
    SUBROUTINE setsize(a,n,m)
      TYPE(matsparse), INTENT(inout) :: a
      INTEGER, INTENT(in) :: n, m
      a%n=n
      a%m=m
    END SUBROUTINE setsize
    ! fonction d'affichage d'une matrice
    SUBROUTINE affiche(a)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a
      INTEGER :: i
      PRINT *, "matrice de taille", a%n, a%m
      PRINT *, "nnz", SIZE(a%i)
      DO i=1,SIZE(a%i)
        PRINT *,a%i(i),a%j(i),a%val(i)
      END DO
    END SUBROUTINE affiche
    ! fonction de copie d'une matrice
    !     la matrice resultat doit etre vide
    SUBROUTINE spcopy(a,b)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(out) :: a
      TYPE(matsparse), INTENT(in) :: b
      INTEGER :: nnz

      IF (b%isAllocated.EQV..TRUE.) THEN
        nnz=SIZE(b%i)
        CALL sparse(a,b%n,b%m,nnz)
        a%i=b%i
        a%j=b%j
        a%val=b%val
      ELSE
        CALL sparse(a,b%n,b%m)
      END IF
      a%isAllocated=b%isAllocated
    END SUBROUTINE spcopy
    ! fonction d'affectation de valeur a tous les coefficients d'une matrice
    SUBROUTINE spaffect(a,b)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(out) :: a
      REAL(kind=8), INTENT(in) :: b
      a%val=b
    END SUBROUTINE spaffect
    ! recherche la position d'un coefficient
    FUNCTION findpos(a, i, j, mo) RESULT(ind)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a
      INTEGER, INTENT(in) :: i, j
      INTEGER, INTENT(in), OPTIONAL :: mo
      INTEGER :: ind, nnz, k, m
      
      nnz=SIZE(a%i)
      m=nnz
      IF (PRESENT(mo)) THEN
        m=mo
      END IF
      ind=0
      DO k=1,m
        IF ((a%i(k) == i).AND.(a%j(k) == j)) THEN
          ind=k
          EXIT
        END IF
      END DO
    END FUNCTION findpos
    ! recherche des coefficients correspondant a un critere
    FUNCTION findind(a, mask) RESULT(ind)
      IMPLICIT NONE
      LOGICAL, DIMENSION(:), INTENT(in) :: mask
      TYPE(matsparse), INTENT(in) :: a
      INTEGER, DIMENSION(:), POINTER :: ind
      INTEGER :: nnz, k, m
      
      nnz=SIZE(a%i)
      IF (nnz /= SIZE(mask)) THEN
        STOP "Erreur, dimension incompatible du mask"
      END IF
      
      m=COUNT(mask)
      ALLOCATE(ind(m))
      ind=PACK((/(k,k=1,nnz)/),mask)
    END FUNCTION findind
    ! extrait les coefficients ayant une certaine valeur
    ! permet par exemple de ne recuperer que les coefficients reellement non nuls
    FUNCTION extract(a,mask) RESULT(b)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a
      LOGICAL, DIMENSION(:), INTENT(in) :: mask
      TYPE(matsparse) :: b
      INTEGER :: nnza, nnzb
      
      nnza=SIZE(a%i)
      IF (nnza /= SIZE(mask)) THEN
        STOP "Erreur, dimension incompatible du mask"
      END IF
      nnzb=COUNT(mask)
      CALL sparse(b,a%n,a%m,nnzb)
      b%i=PACK(a%i,mask)
      b%j=PACK(a%j,mask)
      b%val=PACK(a%val,mask)
    END FUNCTION extract
    ! tri d'une matrice
    SUBROUTINE sort(a, sens)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(inout) :: a
      INTEGER, OPTIONAL, INTENT(in) :: sens
      REAL(kind=8) :: tmp
      INTEGER :: itmp, iglob, jglob, k, choix
      LOGICAL :: ok
      choix=1
      IF (PRESENT(sens).AND.((sens==0).OR.(sens==1))) THEN
        choix=sens
      END IF
      DO ! Tri
        ok = .TRUE.
        DO k=2,SIZE(a%i)
          ! calcul des indices globaux
          IF (choix==1) THEN
            iglob=(a%i(k-1)-1)*a%n+a%j(k-1)
            jglob=(a%i(k)-1)*a%n+a%j(k)
          ELSE
            iglob=(a%j(k-1)-1)*a%m+a%i(k-1)
            jglob=(a%j(k)-1)*a%m+a%i(k)           
          END IF
          IF (iglob > jglob) THEN
            ok = .FALSE.
            ! echange des indices de ligne
            itmp=a%i(k-1)
            a%i(k-1)=a%i(k)
            a%i(k)=itmp
            ! echange des indices de colonne
            itmp=a%j(k-1)
            a%j(k-1)=a%j(k)
            a%j(k)=itmp
            ! echange des valeurs
            tmp=a%val(k-1)
            a%val(k-1)=a%val(k)
            a%val(k)=tmp
          END IF
        END DO
        IF (ok) EXIT
      END DO
    END SUBROUTINE sort
    ! transposition d'une matrice
    FUNCTION sptranspose(a) RESULT(b)
      TYPE(matsparse), INTENT(in) :: a
      TYPE(matsparse) :: b

      CALL sparse(b,a%m,a%n,SIZE(a%i))
      b%i=a%j
      b%j=a%i
      b%val=a%val
      CALL sort(b)
    END FUNCTION sptranspose
    ! addition de deux matrices (standard)
    FUNCTION spadd(a,b) RESULT(c)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a, b
      TYPE(matsparse) :: c, tmp
      INTEGER :: k, l, nnz, nnzmax
      INTEGER, DIMENSION(SIZE(b%i)) :: done, ind
      INTEGER, DIMENSION(:), ALLOCATABLE :: indfinal
      LOGICAL :: found
      
      IF ((a%n /= b%n).OR.(a%m /= b%m)) THEN
        STOP "dimensions de matrices incompatibles"
      END IF
      
      done=1
      ind=(/(k,k=1,SIZE(b%i))/)
      nnzmax=SIZE(a%i)+SIZE(b%i)

      CALL sparse(tmp,a%n,a%m,nnzmax)
      k=1
      nnz=1
      DO k=1,SIZE(a%i)
        found=.FALSE.
        l=1
        DO WHILE(l<=SIZE(b%i) .AND. (found .EQV. .FALSE.))
          IF((a%i(k) == b%i(l)) .AND. (a%j(k) == b%j(l))) THEN
            IF((a%val(k)+b%val(l)) /= 0.d0) THEN
              tmp%i(nnz)=a%i(k)
              tmp%j(nnz)=a%j(k)
              tmp%val(nnz)=a%val(k)+b%val(l)
              nnz=nnz+1
            END IF
            found=.TRUE.
            done(l)=0
          END IF
          l=l+1
        END DO
        IF (found .EQV. .FALSE.) THEN
          tmp%i(nnz)=a%i(k)
          tmp%j(nnz)=a%j(k)
          tmp%val(nnz)=a%val(k)
          nnz=nnz+1
        END IF
      END DO
      
      k=SUM(done)
      IF(k/=0) THEN
        ALLOCATE(indfinal(k))
        indfinal=PACK(ind,done==1)
        DO l=1,k
          tmp%i(nnz)=b%i(indfinal(l))
          tmp%j(nnz)=b%j(indfinal(l))
          tmp%val(nnz)=b%val(indfinal(l))
          nnz=nnz+1         
        END DO
        DEALLOCATE(indfinal)
      END IF
      CALL sparse(c,a%n,a%m,nnz-1)
      c%i=tmp%i(1:nnz-1)
      c%j=tmp%j(1:nnz-1)
      c%val=tmp%val(1:nnz-1)
      DEALLOCATE(tmp%i,tmp%j,tmp%val)
    END FUNCTION spadd
    ! addition de deux matrices (par tri et fusion)
    FUNCTION spadd2(a,b) RESULT (c)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a, b
      TYPE(matsparse) :: c, tmp
      INTEGER :: k, l, nnza,nnzb, nnzmax, nnz, iglob, jglob
      LOGICAL, DIMENSION(:), ALLOCATABLE ::mask
      
      IF ((a%n /= b%n).OR.(a%m /= b%m)) THEN
        STOP "dimensions de matrices incompatibles"
      END IF

      nnza=SIZE(a%i)
      nnzb=SIZE(b%i)
      nnzmax=nnza+nnzb
      ! print *, nnza, nnzb, nnzmax
      ALLOCATE(tmp%i(nnzmax),tmp%j(nnzmax),tmp%val(nnzmax),mask(nnzmax))
      tmp%n=a%n
      tmp%m=a%m
      tmp%i(1:nnza)=a%i
      tmp%i(nnza+1:nnzmax)=b%i
      tmp%j(1:nnza)=a%j
      tmp%j(nnza+1:nnzmax)=b%j
      tmp%val(1:nnza)=a%val
      tmp%val(nnza+1:nnzmax)=b%val
      CALL sort(tmp)
      
      DO k=2,nnzmax
        iglob=(tmp%i(k-1)-1)*tmp%n + tmp%j(k-1)
        jglob=(tmp%i(k)-1)*tmp%n + tmp%j(k)
        IF (iglob == jglob) THEN
          tmp%val(k-1)=tmp%val(k-1)+tmp%val(k)
          tmp%val(k)=0.d0
        END IF
      END DO
      
      mask=tmp%val/=0.d0
      nnz=COUNT(mask)
      ALLOCATE(c%i(nnz),c%j(nnz),c%val(nnz))
      c%n=a%n
      c%m=a%m
      c%i=PACK(tmp%i,mask)
      c%j=PACK(tmp%j,mask)
      c%val=PACK(tmp%val,mask)
      DEALLOCATE(mask,tmp%i,tmp%j,tmp%val)
    END FUNCTION spadd2
    ! addition de deux matrices pre-triees
    FUNCTION spaddsort(a,b) RESULT(c)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a, b
      TYPE(matsparse) :: c, tmp
      INTEGER :: k, l, nnz, nnzmax,nnza, nnzb, i

      IF ((a%n /= b%n).OR.(a%m /= b%m)) THEN
        STOP "dimensions de matrices incompatibles"
      END IF
      nnza=SIZE(a%i)
      nnzb=SIZE(b%i)
      nnzmax=nnza+nnzb
      
      ALLOCATE(tmp%i(nnzmax),tmp%j(nnzmax),tmp%val(nnzmax))   
      k=1
      l=1
      nnz=1
      DO WHILE ((k <= nnza) .AND. (l <= nnzb))
        IF (a%i(k) < b%i(l)) THEN
          tmp%i(nnz)=a%i(k)
          tmp%j(nnz)=a%j(k)
          tmp%val(nnz)=a%val(k)
          k=k+1
          nnz=nnz+1
        ELSE IF (a%i(k) == b%i(l)) THEN
          IF (a%j(k) < b%j(l)) THEN
            tmp%i(nnz)=a%i(k)
            tmp%j(nnz)=a%j(k)
            tmp%val(nnz)=a%val(k)
            k=k+1
            nnz=nnz+1
          ELSE IF (a%j(k) == b%j(l)) THEN
            IF ((a%val(k) + b%val(l)) /= 0.d0) THEN
              tmp%i(nnz)=a%i(k)
              tmp%j(nnz)=a%j(k)
              tmp%val(nnz)=a%val(k) + b%val(l)
              nnz=nnz+1
            END IF
            k=k+1
            l=l+1
          ELSE
          tmp%i(nnz)=b%i(l)
          tmp%j(nnz)=b%j(l)
          tmp%val(nnz)=b%val(l)
          l=l+1
          nnz=nnz+1
          END IF
        ELSE
          tmp%i(nnz)=b%i(l)
          tmp%j(nnz)=b%j(l)
          tmp%val(nnz)=b%val(l)
          l=l+1
          nnz=nnz+1
        END IF
      END DO
      
      ! traitement final derniers elements si positionne a un endroit different
      IF(l <= nnzb) THEN
        DO i=l,nnzb
          tmp%i(nnz)=b%i(i)
          tmp%j(nnz)=b%j(i)
          tmp%val(nnz)=b%val(i)
          nnz=nnz+1
        END DO
      END IF
      IF (k <= nnza) THEN
        DO i=k,nnza
          tmp%i(nnz)=a%i(i)
          tmp%j(nnz)=a%j(i)
          tmp%val(nnz)=a%val(i)
          nnz=nnz+1
        END DO
      END IF
      
      ALLOCATE(c%i(nnz-1),c%j(nnz-1),c%val(nnz-1))
      c%n=a%n
      c%m=a%m
      c%i=tmp%i(1:nnz-1)
      c%j=tmp%j(1:nnz-1)
      c%val=tmp%val(1:nnz-1)
      DEALLOCATE(tmp%i,tmp%j,tmp%val)
    END FUNCTION spaddsort
    ! soustraction de deux matrices
    FUNCTION spminus(a,b) RESULT (c)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a, b
      TYPE(matsparse) :: c, tmp
      INTEGER :: k, l, nnza,nnzb, nnzmax, nnz, iglob, jglob
      LOGICAL, DIMENSION(:), ALLOCATABLE ::mask
      
      IF ((a%n /= b%n).OR.(a%m /= b%m)) THEN
        STOP "dimensions de matrices incompatibles"
      END IF
      
      nnza=SIZE(a%i)
      nnzb=SIZE(b%i)
      nnzmax=nnza+nnzb

      CALL sparse(tmp,a%n,a%m,nnzmax)
      tmp%i(1:nnza)=a%i
      tmp%i(nnza+1:nnzmax)=b%i
      tmp%j(1:nnza)=a%j
      tmp%j(nnza+1:nnzmax)=b%j
      tmp%val(1:nnza)=a%val
      tmp%val(nnza+1:nnzmax)=-b%val
      CALL sort(tmp)
      
      DO k=2,nnzmax
        iglob=(tmp%i(k-1)-1)*tmp%n + tmp%j(k-1)
        jglob=(tmp%i(k)-1)*tmp%n + tmp%j(k)
        IF (iglob == jglob) THEN
          tmp%val(k-1)=tmp%val(k-1)-tmp%val(k)
          tmp%val(k)=0.d0
        END IF
      END DO
      
      mask=tmp%val/=0.d0
      nnz=COUNT(mask)
      CALL sparse(c,a%n,a%m,nnz)
      c%i=PACK(tmp%i,mask)
      c%j=PACK(tmp%j,mask)
      c%val=PACK(tmp%val,mask)
      DEALLOCATE(mask,tmp%i,tmp%j,tmp%val)
    END FUNCTION spminus
    ! effectue le produit d'une matrice par un scalaire
    FUNCTION spscalmat(a,b) RESULT(c)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a
      TYPE(matsparse) :: c
      REAL(kind=8), INTENT(in) :: b
      INTEGER :: nnz
      IF(b==0.d0) THEN
        STOP "multiplication d'une sparse par 0"
      END IF
      nnz=SIZE(a%i)
      
      CALL sparse(c,a%n,a%m,nnz)
      c%i=a%i
      c%j=a%j
      c%val=b*a%val
    END FUNCTION spscalmat
    ! effectue le produit d'un scalaire par une matrice
    FUNCTION spmatscal(b,a) RESULT(c)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a
      TYPE(matsparse) :: c
      REAL(kind=8), INTENT(in) :: b
      INTEGER :: nnz

      IF(b==0.d0) THEN
        STOP "multiplication d'une sparse par 0"
      END IF
      nnz=SIZE(a%i)
      
      CALL sparse(c,a%n,a%m,nnz)
      c%i=a%i
      c%j=a%j
      c%val=b*a%val
    END FUNCTION spmatscal
    ! effectue le produit d'une matrice par un vecteur
    FUNCTION spmatvec(a,b) RESULT(u)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a
      REAL(kind=8), DIMENSION(:), INTENT(in) :: b
      REAL(kind=8), DIMENSION(a%n) :: u
      INTEGER :: i,nnz
      
      IF (a%m /= SIZE(b)) THEN
        STOP "dimensions matrice / vecteur incompatibles"
      END IF
      u=0.d0
      nnz=SIZE(a%i)
      DO i=1,nnz
        u(a%i(i))=u(a%i(i))+a%val(i)*b(a%j(i))
      END DO
    END FUNCTION spmatvec
    ! effectue le produit d'une matrice par une matrice
    FUNCTION spmatmat(a,b) RESULT(u)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: a, b
      TYPE(matsparse) :: u,tmp
      INTEGER :: i,j,k,nnz,nnzmax,nnza,nnzb
      
      IF (a%m /= b%n) THEN
        STOP "dimensions de matrices incompatibles"
      END IF
      nnza=SIZE(a%i)
      nnzb=SIZE(b%i)
      nnzmax=nnza*nnzb
      CALL sparse(tmp,a%n,a%m,nnzmax)
      tmp%val=0.d0
      nnz=1
      DO i=1,nnza
        DO j=1,nnzb
          IF (a%j(i) == b%i(j)) THEN
            k=find(tmp,a%i(i),b%j(j),nnz-1)
            IF(k/=0) THEN
              tmp%i(k)=a%i(i)
              tmp%j(k)=b%j(j)
              tmp%val(k)=tmp%val(k)+a%val(i)*b%val(j)
            ELSE
            tmp%i(nnz)=a%i(i)
              tmp%j(nnz)=b%j(j)
              tmp%val(nnz)=tmp%val(nnz)+a%val(i)*b%val(j)
              nnz=nnz+1
            END IF
          END IF
        END DO
      END DO
      u=extract(tmp,tmp%val/=0.d0)
      DEALLOCATE(tmp%i,tmp%j,tmp%val)
    END FUNCTION spmatmat
    ! lufact - calcule factorisation LU: A=L*U
    SUBROUTINE lufact(A, L, U, klo, kuo)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: A
      TYPE(matsparse), INTENT(out) :: L, U
      TYPE(matsparse) :: Ltmp, Utmp
      INTEGER, OPTIONAL, INTENT(in) :: klo, kuo
      INTEGER :: i, j, k, nu, nl, n, nnzl, nnzu, kl, ku, ind1, ind2

      n=A%n
      
      kl=A%n-1
      IF(PRESENT(klo).AND.klo<n) THEN
        kl=klo
      END IF
      
      ku=A%n-1
      IF(PRESENT(kuo).AND.kuo<n) THEN
        ku=kuo
      END IF
      nnzl=(kl+1)*(2*n-kl)/2
      nnzu=(ku+1)*(2*n-ku)/2
      CALL sparse(Ltmp, A%n, A%m, nnzl)
      CALL sparse(Utmp, A%n, A%m, nnzu)
      nnzl=1
      nnzu=1
      
      DO k=1,n
        ! traitement du coefficient diagonal
        Ltmp%i(nnzl)=k
        Ltmp%j(nnzl)=k
        Ltmp%val(nnzl)=1.d0
        nnzl=nnzl+1
        ind1=find(A,k,k)
        IF (ind1/=0) THEN
          Utmp%val(nnzu)=A%val(ind1)
        ELSE
          Utmp%val(nnzu)=0.d0
        END IF
        DO j=1,k-1
          ind1=find(Ltmp,k,j,nnzl)
          ind2=find(Utmp,j,k,nnzu)
          IF (ind1/=0.AND.ind2/=0) THEN
            Utmp%val(nnzu)=Utmp%val(nnzu)-Ltmp%val(ind1)*Utmp%val(ind2)
          END IF
        END DO
        IF(Utmp%val(nnzu)/=0.d0) THEN
          Utmp%i(nnzu)=k
          Utmp%j(nnzu)=k
          nnzu=nnzu+1
        END IF
        
        ! traitement des coefficients sous-diagonaux de L
        nl=MIN(k+kl,n)
        DO i=k+1,nl
          ind1=find(A,i,k)
          IF(ind1/=0) THEN
            Ltmp%val(nnzl)=A%val(ind1)
          END IF
          DO j=1,k-1
            ind1=find(Ltmp,i,j,nnzl)
            ind2=find(Utmp,j,k,nnzu)

            IF (ind1/=0.AND.ind2/=0) THEN
              Ltmp%val(nnzl)=Ltmp%val(nnzl)-Ltmp%val(ind1)*Utmp%val(ind2)
            END IF
          END DO
          IF(Ltmp%val(nnzl)/=0.d0) THEN
            ind1=find(Utmp,k,k,nnzu)
            Ltmp%i(nnzl)=i
            Ltmp%j(nnzl)=k
            Ltmp%val(nnzl)=Ltmp%val(nnzl)/Utmp%val(ind1)
            nnzl=nnzl+1
          END IF
        END DO
        
        ! traitement des coefficients sur-diagonaux de U
        nu=MIN(k+ku,n)
        DO i=k+1,nu
          ind1=find(A,k,i)
          IF(ind1/=0) THEN
            Utmp%val(nnzu)=A%val(ind1)
          ELSE
            Utmp%val(nnzu)=0.d0
          END IF
          DO j=1,k-1
            ind1=find(Ltmp,k,j,nnzl)
            ind2=find(Utmp,j,i,nnzu)
            IF (ind1/=0.AND.ind2/=0) THEN
              Utmp%val(nnzu)=Utmp%val(nnzu)-Ltmp%val(ind1)*Utmp%val(ind2)
            END IF
          END DO
          IF(Utmp%val(nnzu)/=0.d0) THEN
            Utmp%i(nnzu)=k
            Utmp%j(nnzu)=i
            nnzu=nnzu+1
          END IF
        END DO
      END DO
      L=Ltmp
      U=Utmp
      ! numeriquement un coeff nul est un coeff plus petit,
      ! en valeur absolue, que le zero machine
      ! L=extract(Ltmp,dabs(Ltmp%val)>epsilon(1.d0))
      ! U=extract(Utmp,dabs(Utmp%val)>epsilon(1.d0))
      DEALLOCATE(Ltmp%i,Ltmp%j,Ltmp%val)
      DEALLOCATE(Utmp%i,Utmp%j,Utmp%val)
    END SUBROUTINE lufact
    ! lusolve - resout le systeme L*U x = f par L*y=f puis U*x=y
    SUBROUTINE lusolve(L,U,f,x,klo,kuo)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: L, U
      REAL(kind=8), DIMENSION(:), INTENT(in) :: f
      REAL(kind=8), DIMENSION(SIZE(f)), INTENT(out) :: x
      REAL(kind=8), DIMENSION(SIZE(f)) :: y
      INTEGER, OPTIONAL, INTENT(in) :: klo, kuo
      INTEGER :: i, j, n, kl, ku, nl, nu, ind
      
      n=SIZE(f)
      
      kl=SIZE(f)-1
      IF(PRESENT(klo).AND.klo<n) THEN
        kl=klo
      END IF
      
      ku=SIZE(f)-1
      IF(PRESENT(kuo).AND.kuo<n) THEN
        ku=kuo
      END IF
      
      x=0.d0
      y=0.d0
      
      ! resolution de L*y=f
      DO i=1,n
        y(i)=f(i)
        nl=MAX(1,i-kl)
        DO j=nl,i-1
          ind=find(L,i,j)
          IF(ind/=0) THEN
            y(i)=y(i)-L%val(ind)*y(j)
          END IF
        END DO
      END DO
      
      ! resolution de U x = y
      DO i=n,1,-1
        x(i)=y(i)
        nu=MIN(n,i+ku)
        DO j=i+1,nu
          ind=find(U,i,j)
          IF (ind/=0) THEN
            x(i)=x(i)-U%val(ind)*x(j)
          END IF
        END DO
        ind=find(U,i,i)
        x(i)=x(i)/U%val(ind)
      END DO
    END SUBROUTINE lusolve

    ! MatJacobi - On decompose la matrice A=M-N pour Jacobi
    ! On calcule ausi J=inv(M)N la matrice d'iterations
    SUBROUTINE MatJacobi(A,M,N,J)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: A
      TYPE(matsparse), INTENT(out) :: M,N,J
      TYPE(matsparse) :: JJ
      INTEGER :: k
      REAL(8) :: val

      CALL sparse(M,A%n,A%n)
      CALL sparse(N,A%n,A%n)
      CALL sparse(JJ,A%n,A%n)

      DO k=1,A%n
         val=coeff(A,k,k)
         IF (val==0.d0) THEN
            STOP "ATTENTION Jacobi ne va pas marcher"
         END IF
         CALL setcoeff(M,k,k,val)
         CALL setcoeff(JJ,k,k,1/val)
      END DO
      
      N=M+(-1.d0)*A
      J=JJ*N
    END SUBROUTINE MatJacobi
    
    ! On décompose la matrice A=M-N pour Gauss-Seidel
    SUBROUTINE MatGauss(A,M,N)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in) :: A
      TYPE(matsparse), INTENT(out) :: M,N
      INTEGER :: k,l
      REAL(8) :: val,val2
      
      CALL sparse(M,A%n,A%n)
      CALL sparse(N,A%n,A%n)
      
      DO k=1,A%n
         val2=coeff(A,k,k)
         IF (val2==0.d0) THEN
            STOP "ATTENTION Gauss-Seidel ne va pas marcher"
         END IF
         DO l=1,k
            val=coeff(A,k,l)
            IF (val /= 0.d0) THEN
            CALL setcoeff(M,k,l,val)
            END IF
         END DO
      END DO
      
      N=M+(-1.d0)*A

    END SUBROUTINE MatGauss

    ! On va résoudre (M-N)x=f par Jacobi
    SUBROUTINE Jacsolve(M,N,x,f)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in)::M,N
      REAL(kind=8), DIMENSION(:), INTENT(in)::f
      REAL(kind=8), DIMENSION(size(f)), INTENT(out) :: x
      REAL(kind=8), DIMENSION(size(f)) :: y,temp
      REAL(kind=8) :: eps,val,r,r0
      INTEGER :: nn,i,ii,j,it,it_max

      nn=size(f)
      
      x=0.d0
      y=0.d0
      r0=0.d0
      it=0
      
      DO ii=1,nn
         r0=r0+f(i)**2
      END DO

      eps=10E-5
      it_max=300

      DO WHILE( dsqrt(r) > eps*dsqrt(r0) .AND. it<it_max )
         it=it+1
         y=N*x+f
         DO j=1,nn
            val=coeff(M,j,j)
            x(j)=y(j)/val
         END DO
         r=0.d0
         DO i=1,nn
            temp=(M+(-1.d0)*N)*x
            r=r+(f(i)-temp(i))**2
         END DO
      END DO
      PRINT *, "Nombre d'iterations de Jacobi ",it

    END SUBROUTINE Jacsolve
    
    ! definition de la matrice identite sparse
    FUNCTION speye(n) RESULT(a)
      IMPLICIT NONE
      INTEGER, INTENT(in) :: n
      TYPE(matsparse) :: a
      INTEGER :: i
      
      ALLOCATE(a%i(n),a%j(n),a%val(n))
      a%n=n
      a%m=n
      a%i=(/(i,i=1,n)/)
      a%j=(/(i,i=1,n)/)
      a%val=1.d0
    END FUNCTION speye

    ! On va résoudre (M-N)x=f par Gauss-Seidel
    SUBROUTINE Gaussolve(M,N,x,f)
      IMPLICIT NONE
      TYPE(matsparse), INTENT(in)::M,N
      TYPE(matsparse) :: Id
      REAL(kind=8), DIMENSION(:), INTENT(in) :: f
      REAL(kind=8), DIMENSION(size(f)), INTENT(out) :: x
      REAL(kind=8), DIMENSION(size(f)) :: y,temp
      REAL(8) :: eps,val,r,r0
      INTEGER :: nn,i,ii,it,it_max,j,jj,ind,nl
      
      nn=size(f)
      
      x=0.d0
      y=0.d0
      r0=0.d0
      it=0
      
      Id=speye(nn)
      
      DO ii=1,nn
         r0=r0+f(i)**2
      END DO

      eps=10E-5
      it_max=300

      DO WHILE( dsqrt(r) > eps*dsqrt(r0) .AND. it<it_max )
         it=it+1
         y=N*x+f
         ! resolution de M*x=y
         DO j=1,nn
            x(j)=y(j)
            DO jj=1,j-1
               ind=find(M,j,jj)
               IF(ind/=0) THEN
                  x(j)=x(j)-M%val(ind)*x(jj)
               END IF
            END DO
            ind=find(M,j,j)
            x(j)=x(j)/M%val(ind)
         END DO
         

         r=0.d0
         DO i=1,nn
            temp=(M+(-1.d0)*N)*x
            r=r+(f(i)-temp(i))**2
         END DO
      END DO
      PRINT *, "Nombre d'iterations de Gauss-Seidel ",it
      PRINT *, "residus gauss-seidel r=",r

    END SUBROUTINE Gaussolve



    ! routine de test
    SUBROUTINE test_amsta01sparse()
      TYPE(matsparse) :: mat, mat2, mat3, mat4, mat5, mat6, mat7, mat8, mat9,mat10,mat11,mat12,mat13,mat14,L, U, M,NN,J,res
      INTEGER, PARAMETER :: n=10
      REAL(kind=8) :: eps
      INTEGER, DIMENSION(:), ALLOCATABLE :: ind, indth
      REAL(kind=8), DIMENSION(:), ALLOCATABLE :: x,xth, b
      INTEGER :: i
      ! recherche du zero machine
      eps=EPSILON(1.d0)
      
      PRINT *,
      PRINT *, "-------------- TEST MODULE AMSTA01SPARSE -----------"
      PRINT *,
      
      PRINT *, "------------------------------------------"
      PRINT *, "Test setcoeff"
      
      CALL sparse(mat,2,2)
      CALL setcoeff(mat,1,1,2.d0)
      CALL sparse(res,2,2,1)
      res%i(1)=1
      res%j(1)=1
      res%val(1)=2.d0
      IF (dsqrt(SUM((mat%i-res%i)*(mat%i-res%i)+(mat%j-res%j)*(mat%j-res%j)+(mat%val-res%val)*(mat%val-res%val))) < eps) THEN
        PRINT *, "     test setcoeff - first coeff OK"
      ELSE
        PRINT *, "     erreur setcoeff - first coeff"
      END IF
      
      CALL setcoeff(mat,1,2,-1.d0)
      DEALLOCATE(res%i,res%j,res%val)
      CALL sparse(res,2,2,2)
      res%i=(/1,1/)
      res%j=(/1,2/)
      res%val=(/2.d0,-1.d0/)
      
      IF (dsqrt(SUM((mat%i-res%i)*(mat%i-res%i)+(mat%j-res%j)*(mat%j-res%j)+(mat%val-res%val)*(mat%val-res%val))) < eps) THEN
        PRINT *, "     test setcoeff - insertion OK"
      ELSE
        PRINT *, "     error setcoeff - insertion"
      END IF
      
      CALL setcoeff(mat,1,2,1.d0)
      res%i=(/1,1/)
      res%j=(/1,2/)
      res%val=(/2.d0,1.d0/)
      IF (dsqrt(SUM((mat%i-res%i)*(mat%i-res%i)+(mat%j-res%j)*(mat%j-res%j)+(mat%val-res%val)*(mat%val-res%val))) < eps) THEN
        PRINT *, "     test setcoeff - update OK"
      ELSE
        PRINT *, "     error setcoeff - update"
      END IF

      PRINT *, "------------------------------------------"
      PRINT *, "Test addition"
      CALL msreallocate(mat,5)
      CALL setsize(mat,3,3)
      mat%i=(/ 2, 1, 2, 2, 3/)
      mat%j=(/ 1, 1, 3, 2, 1/)
      mat%val=(/ 2.d0, 1.d0, -2.d0, -1.d0, 1.d0/)
      CALL sparse(mat2,3,3,6)
      mat2%i=(/ 2, 1, 3, 1, 3, 3/)
      mat2%j=(/ 1, 1, 1, 2, 3, 2/)
      mat2%val=(/ -2.d0, 2.d0, 2.d0, 3.d0, -1.d0, 3.d0/)
      
      mat3=spadd(mat,mat2)
      CALL msreallocate(res,7)
      res%i=(/ 1, 2, 2, 3, 1, 3, 3 /)
      res%j=(/ 1, 3, 2, 1, 2, 3, 2 /)
      res%val=(/ 3.d0, -2.d0, -1.d0, 3.d0, 3.d0, -1.d0, 3.d0/)
      
      IF(dsqrt(SUM((mat3%i-res%i)*(mat3%i-res%i)+(mat3%j-res%j)*(mat3%j-res%j)+(mat3%val-res%val)*(mat3%val-res%val))) < eps) THEN
        PRINT *, "     test addition - unsorted OK"
      ELSE
        PRINT *, "     error addition - unsorted"
      END IF

      mat4=spadd2(mat,mat2)
      res%i=(/ 1, 1, 2, 2, 3, 3, 3 /)
      res%j=(/ 1, 2, 2, 3, 1, 2, 3 /)
      res%val=(/ 3.d0, 3.d0, -1.d0, -2.d0, 3.d0, 3.d0, -1.d0/)
      
      IF(dsqrt(SUM((mat4%i-res%i)*(mat4%i-res%i)+(mat4%j-res%j)*(mat4%j-res%j)+(mat4%val-res%val)*(mat4%val-res%val))) < eps) THEN
        PRINT *, "     test addition - sort and merge OK"
      ELSE
        PRINT *, "     error addition - sort and merge"
      END IF
      
      CALL sort(mat)
      CALL sort(mat2)
      mat5=spaddsort(mat2,mat)
      res%i=(/ 1, 1, 2, 2, 3, 3, 3 /)
      res%j=(/ 1, 2, 2, 3, 1, 2, 3 /)
      res%val=(/ 3.d0, 3.d0, -1.d0, -2.d0, 3.d0, 3.d0, -1.d0/)
      
      IF(dsqrt(SUM((mat5%i-res%i)*(mat5%i-res%i)+(mat5%j-res%j)*(mat5%j-res%j)+(mat5%val-res%val)*(mat5%val-res%val))) < eps) THEN
        PRINT *, "     test addition - sorted OK"
      ELSE
        PRINT *, "     error addition - sorted"
      END IF
      
      PRINT *, "------------------------------------------"
      PRINT *, "Test findind"
      ind=find(mat5,mat5%val==3.d0)
      ALLOCATE(indth(4))
      indth=(/ 1, 2, 5, 6 /)
      IF(SIZE(ind)==SIZE(indth) .AND. dsqrt(DBLE(SUM((indth-ind)*(indth-ind)))) < eps) THEN
        PRINT *, "     test findind OK"
      ELSE
        PRINT *, "     erreur findind"
      END IF
      
      PRINT *, "------------------------------------------"
      PRINT *, "Test findpos"
      i=find(mat5,2,3)
      IF (i==4) THEN
        PRINT "('      test findpos OK : i=2, j=3, imax=7, k=',i1,'=4')",i
      ELSE
        PRINT "('      erreur findpos : i=2, j=3, imax=7, k=',i1,'/=4')",i
      END IF
      i=find(mat5,2,3,3)
      IF (i==0) THEN
        PRINT "('      test findpos OK : i=2, j=3, imax=3, k=',i1,'=0')",i
      ELSE
        PRINT "('      erreur findpos : i=2, j=3, imax=3, k=',i1,'/=0')",i
      END IF
      i=find(mat5,1,3)
      IF (i==0) THEN
        PRINT "('      test findpos OK : i=1, j=3, imax=7, k=',i1,'=0')",i
      ELSE
        PRINT "('      erreur findpos : i=1, j=3, imax=7, k=',i1,'/=0')",i
      END IF
      
      PRINT *, "------------------------------------------"
      PRINT *,"Test lufact"
      CALL msreallocate(mat,4)
      CALL msreallocate(mat2,5)

      mat%i=(/ 1, 2, 2, 3 /)
      mat%j=(/ 1, 1, 2, 3 /)
      mat%val=(/ 1.d0, 2.d0, 1.d0, 1.d0 /)
      mat2%i=(/ 1, 1, 2, 2, 3 /)
      mat2%j=(/ 1, 3, 2, 3, 3 /)
      mat2%val=(/ -1.d0, 1.d0, -2.d0, 1.d0, -1.d0 /)

      mat6=mat*mat2
      
      CALL lufact(mat6,L,U)
      mat7=(-1.d0)*(L*U)
      mat8=mat7+mat6
      IF( dsqrt(SUM((mat8%val)*(mat8%val))) > eps) THEN
        PRINT *, "     error lufact"
      ELSE
        PRINT *, "     test lufact OK"
      END IF

      
      
      PRINT *, "------------------------------------------"

      PRINT *, "------------------------------------------"
      PRINT *,"Test MatJacobi"
      CALL msreallocate(mat,4)
      CALL msreallocate(mat2,5)

      mat%i=(/ 1, 2, 2, 3 /)
      mat%j=(/ 1, 1, 2, 3 /)
      mat%val=(/ 1.d0, 2.d0, 1.d0, 1.d0 /)
      mat2%i=(/ 1, 1, 2, 2, 3 /)
      mat2%j=(/ 1, 3, 2, 3, 3 /)
      mat2%val=(/ -1.d0, 1.d0, -2.d0, 1.d0, -1.d0 /)

      mat9=mat*mat2
      CALL MatJacobi(mat9,M,NN,J)
      mat10=M+(-1.d0)*NN
      mat11=mat9+(-1.d0)*mat10
      
      IF( dsqrt(SUM((mat11%val)*(mat11%val))) > eps) THEN
        PRINT *, "     error MatJacobi"
        PRINT *, "A="
        CALL AFFICHE(mat9)
        PRINT *, "M="
        CALL AFFICHE(M)
        PRINT *, "N="
        CALL AFFICHE(NN)
      ELSE
        PRINT *, "     test MatJacobi OK"
      END IF

 PRINT *, "------------------------------------------"
      PRINT *,"Test MatGauss"
      CALL msreallocate(mat,4)
      CALL msreallocate(mat2,5)

      mat%i=(/ 1, 2, 2, 3 /)
      mat%j=(/ 1, 1, 2, 3 /)
      mat%val=(/ 1.d0, 2.d0, 1.d0, 1.d0 /)
      mat2%i=(/ 1, 1, 2, 2, 3 /)
      mat2%j=(/ 1, 3, 2, 3, 3 /)
      mat2%val=(/ -1.d0, 1.d0, -2.d0, 1.d0, -1.d0 /)

      mat12=mat*mat2
      CALL MatGauss(mat12,M,NN)
      mat13=M+(-1.d0)*NN
      mat14=mat12+(-1.d0)*mat13

      IF( dsqrt(SUM((mat11%val)*(mat11%val))) > eps) THEN
        PRINT *, "     error MatGauss"
      ELSE
        PRINT *, "     test MatGauss OK"
      END IF

      PRINT *, "Test lusolve"
      ALLOCATE(x(3),xth(3),b(3))
      xth=(/ 1.d0, 2.d0, 3.d0 /)
      b=(/ 2.d0, 3.d0, -3.d0 /)
      CALL lusolve(L, U, b, x)
      
      IF(dsqrt(SUM((x-xth)*(x-xth))) > eps) THEN
        PRINT *, "     error lusolve"
      ELSE
        PRINT *, "     test lusolve OK"
      END IF
      DEALLOCATE(x)

      PRINT *, "Test Gaussolve"
      ALLOCATE(x(3))
      CALL Gaussolve(M,NN, x, b)
      
      IF(dsqrt(SUM((x-xth)*(x-xth))) > eps) THEN
        PRINT *, "     error Gaussolve"
      ELSE
        PRINT *, "     test Gaussolve OK"
      END IF
      
      DEALLOCATE(mat%i,mat%j,mat%val)
      DEALLOCATE(mat2%i,mat2%j,mat2%val)
      DEALLOCATE(mat3%i,mat3%j,mat3%val)
      DEALLOCATE(mat4%i,mat4%j,mat4%val)
      DEALLOCATE(mat5%i,mat5%j,mat5%val)
      DEALLOCATE(mat6%i,mat6%j,mat6%val)
      DEALLOCATE(mat7%i,mat7%j,mat7%val)
      DEALLOCATE(mat8%i,mat8%j,mat8%val)
      DEALLOCATE(L%i,L%j,L%val)
      DEALLOCATE(U%i,U%j,U%val)
      DEALLOCATE(ind)
    END SUBROUTINE test_amsta01sparse
END MODULE amsta01sparse
