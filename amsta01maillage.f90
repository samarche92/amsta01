! ----------------------------------------------------------------
! module de lecture de maillages gmsh
! Auteur : N. Kielbasiewicz
! -----------------------------------------------------------------
!use mpi
module amsta01maillage


  type maillage
    integer :: nbNodes, nbElems, nbTri, nbTask,myrank,ierr
    real(kind=8), dimension(:,:), pointer :: coords
    integer, dimension(:,:), pointer :: typeElems, elemsVertices, triVertices
    integer, dimension(:), pointer :: refNodes, refElems, refTri, elemsNbPart, triNbPart, elemsPartRef, triPartRef,TrueNodes,TrueElems
  end type

  contains
    ! retourne le nombre de noeuds d'un type de maille gmsh donne
    function type2nbNodes(t) result(tt)
      implicit none
      integer, intent(in) :: t
      integer :: tt
      integer, dimension(31) :: nbNodesOfTypes
      nbNodesOfTypes = (/2,3,4,4,8,6,5,3,6,9,10,27,18,14,1,8,20,15,13,9,10,12,15,15,21,4,5,6,20,35,56 /)
      tt=nbNodesOfTypes(t)
    end function


    ! construit un maillage par lecture d'un fichier gmsh
    function loadFromMshFile(filename) result(res)

      implicit none
      character(len=*), intent(in) :: filename
      type(maillage) :: res
      integer :: ios, ibuf, ibufD, ibufT, i, j, nbtags
      character(len=100) :: sbuf, sbuf2, sbuf3
      real(kind=8) :: rbuf
      integer, dimension(:), pointer :: elemData

      open(unit=10, file=filename, form='formatted', status='old', iostat=ios)
      if (ios /= 0) then
        stop "ERROR File not opened"
      end if

      do while (ios == 0)

        read(10,*, iostat=ios) sbuf

        if (sbuf == "$MeshFormat") then
          ! read next line containing the file format version, and 2 integers about numeric precision
          read(10,*, iostat=ios) sbuf, sbuf2, sbuf3
          ! check if block ends correctly
          read(10,'(A14)', iostat=ios) sbuf
          if (sbuf /= "$EndMeshFormat") then
            stop "ERROR : Msh $MeshFormat block must end with $EndMeshFormat"
          end if

        else if (sbuf =="$Nodes") then
          ! read number of nodes
          read(10,*, iostat=ios) res%nbNodes
          print*, "nbNodes=",res%nbNodes
          allocate(res%coords(res%nbNodes,3), res%refNodes(res%nbNodes),res%TrueNodes(res%nbNodes))
          res%refNodes=0
          ! read coordinates
          do i=1,res%nbNodes
             res%TrueNodes(i)=i
            read(10,*, iostat=ios) ibuf, res%coords(i,1), res%coords(i,2), res%coords(i,3)
          end do
          ! check if block ends correctly
          read(10,'(A9)', iostat=ios) sbuf
          if (sbuf /= "$EndNodes") then
            stop "ERROR : Msh $Nodes block must end with $EndNodes"
          end if

        else if (sbuf =="$Elements") then
          read(10,*, iostat=ios) res%nbElems
          print*, "nbElems=",res%nbElems
          allocate(res%typeElems(res%nbElems,2), res%refElems(res%nbElems), res%elemsVertices(res%nbElems,3))
          allocate(res%elemsNbPart(res%nbElems), res%elemsPartRef(res%nbElems))
          ! read elements data
          do i=1,res%nbElems

            ! res%TrueElems(i)=i
            ! We get each line in a character string
            read(10,'(A)', iostat=ios) sbuf
            ! convert character string to array of integers
            allocate(elemData(100))
            read(sbuf,*,iostat=ios) elemData
            ! get type of element and number of vertices related to it
            res%typeElems(i,1)=elemData(2)
            res%typeElems(i,2)=type2nbNodes(res%typeElems(i,1))
            ! management of tags (domain ref of element and eventually partition data)
            nbtags=elemData(3)
            res%refElems(i)=elemData(4)
            res%elemsNbPart(i)=elemData(6)
            res%elemsPartRef(i)=elemData(7)
            ! read vertices of element
            do j=1,res%typeElems(i,2)
              res%elemsVertices(i,j)=elemData(nbtags+3+j)
            end do
            ! set reference of vertices
            do j=1,res%typeElems(i,2)
              if (res%refNodes(res%elemsVertices(i,j)) == 0) then
                res%refNodes(res%elemsVertices(i,j)) = res%refElems(i)
              end if
            end do
            deallocate(elemData)
          end do
          ! check if block ends correctly
          read(10,'(A12)', iostat=ios) sbuf
          if (sbuf /= "$EndElements") then
            stop "ERROR : Msh $Elements block must end with $EndElements"
          end if
        else
        end if

      end do
      close(10)
    end function


    ! construit la liste des triangles du maillage
    subroutine getTriangles(mail)
      type(maillage), intent(inout) :: mail
      integer :: i, j, nbTri
      nbTri=0

      do i=1, mail%nbElems
        if (mail%typeElems(i,1) == 2) then
          nbTri=nbTri+1
        end if
      end do
      mail%nbTri=nbTri
      Print*, "NbTri=", NbTri

      allocate(mail%refTri(nbTri), mail%triVertices(nbTri,3), mail%triNbPart(nbTri))
      j=1
      do i=1, mail%nbElems
        if (mail%typeElems(i,1) == 2) then
          mail%refTri(j)=mail%refElems(i)
          mail%triVertices(j,1:3)=mail%elemsVertices(i,1:3)
          mail%triNbPart(j) = mail%elemsNbPart(i)
          j=j+1
        end if
      end do
    end subroutine getTriangles


    ! Assemble l'inter
    ! --------------------
    subroutine assembleInter(mail,inter)
      implicit none
      type(maillage), intent(in) :: mail

      ! On recupere les indices des points
      integer :: i, j, k
      integer :: nb_pts_inter
      integer :: Ok
      ! On cree un vecteur qui contient tous les noeuds de l'inter
      ! On aura besoin d'un vecteur qui contient les points pas sur l'inter
      integer, dimension(:), pointer, intent(out) :: inter
      integer, dimension(:), pointer ::pas_inter

      j = 1

      allocate (pas_inter(mail%nbNodes))
      do i=1, mail%nbNodes
         pas_inter(i) = 0
      end do

      ! Parcours des triangles
      ! On va remplir un vecteur qui contient tous les points pas de l'inter
      do i=1, mail%NbTri
         ! write (*,*) 'Triangle gentil numero ', i
         if (mail%triNbPart(i) == 1) then ! Aucun des sommets du triangle sur l'inter
            do k = 1, 3
               !write (*,*) 'Sommet ', k, (mail%triVertices(i,k))
               ! On s'assure que le sommet est pas deja dans le tableau
               if (all(pas_inter .NE. (mail%triVertices(i,k)))) then
                  pas_inter(j) = mail%triVertices(i, k)
                  !write (*,*) j, mail%triVertices(i, k)
                  j = j+1
               end if
            end do
         end if
      end do

      ! On peut deduire le vecteur inter de ce qui precede
      ! On n'oublie pas de retrancher le 1 ajoute en trop a j a la fin de la boucle
      j = j-1
      nb_pts_inter = mail%nbNodes - j
      !write (*,*) nb_pts_inter
      allocate(inter(nb_pts_inter))
      k = 1
      do i=1, mail%nbNodes
         ! Recherche du sommet i dans pas_inter
         if (all(pas_inter .NE. i)) then
            ! Le sommet est pas dans pas_inter, donc il est dans inter
            inter(k) = i
            k = k+1
         end if
      end do
     
    end subroutine assembleInter

! construit un sous-maillage pour chaque partition
    SUBROUTINE sous_maillage(new_mail,mail,myRank)
      implicit none
      type(maillage), intent(in):: mail
      type(maillage), intent(out):: new_mail
      integer, intent(in) :: myRank
      integer :: i,ii,j,k,kk,l,ll,p,n,m,mm,pp,nt,ind1,ind2,loc,loco,ind11,ind22
      
      new_mail%nbElems=0
      new_mail%nbNodes=0
      ii=0
      kk=0
      ll=0
      mm=0
      !on compte les elements
      do i=1,mail%nbElems
         if (myRank==mail%elemsPartRef(i)) then
            new_mail%nbElems=new_mail%nbElems+1
         end if
      end do

      allocate(new_mail%refElems(new_mail%nbElems))
      allocate(new_mail%typeElems(new_mail%nbElems,2))
      allocate(new_mail%elemsVertices(new_mail%nbElems,3))
      allocate(new_mail%elemsNbPart(new_mail%nbElems))
      !allocate(new_mail%TrueElems(new_mail%nbElems))
      
      !on attrape les infos dessus
      do j=1,mail%nbElems
         if (myRank==mail%elemsPartRef(j)) then
            ii=ii+1
            new_mail%refElems(ii)=mail%refElems(j)
            !new_mail%TrueElems(ii)=j
            new_mail%typeElems(ii,1:2)=mail%typeElems(j,1:2)
            new_mail%elemsVertices(ii,1:3)=mail%elemsVertices(j,1:3)
            new_mail%elemsNbPart(ii)=mail%elemsNbPart(j)
         end if
      end do
      
      ! maintenant il nous faut les noeuds
      ! on commence par les compter
      do l=1,mail%nbNodes
         if(any(new_mail%elemsVertices ==  l)) then
            new_mail%nbNodes=new_mail%nbNodes+1
         end if
      end do

      allocate(new_mail%coords(new_mail%nbNodes,3))
      allocate(new_mail%refNodes(new_mail%nbNodes))
      allocate(new_mail%TrueNodes(new_mail%nbNodes))

      ! Et maintenant on copie les infos dessus
      do p=1,mail%nbNodes
         if(any(new_mail%elemsVertices == p)) then
            ll=ll+1
            new_mail%coords(ll,1:3)=mail%coords(p,1:3)
            new_mail%refNodes(ll)=mail%refNodes(p)
            new_mail%TrueNodes(ll)=p
         end if
      end do

      ! En fait le new_mail%triVertices d'avant est faux, puisqu'il
      ! contient des refernces aux anciens noeuds. Il faut reparer ça.
      do ind1=1,new_mail%nbElems
         do ind2=1,new_mail%typeElems(ind1,2)
            loc = minloc(abs(new_mail%TrueNodes - new_mail%elemsVertices(ind1,ind2)), 1)
            new_mail%elemsVertices(ind1,ind2)=loc
         end do
      end do

      ! si le rang a jamais ete egal a un numero de partition,
         ! c'est qu'on est sur l'interface
      ! commençons par compter les éléments
      if (new_mail%nbElems==0) then
         print *, '*************************'
         print *, 'Interface'

         do k=1,mail%nbElems
           if(mail%elemsNbPart(k).gt.1) then
              new_mail%nbElems=new_mail%nbElems+1
           end if
        end do

        allocate(new_mail%refElems(new_mail%nbElems))
        allocate(new_mail%typeElems(new_mail%nbElems,2))
        allocate(new_mail%elemsVertices(new_mail%nbElems,3))
        allocate(new_mail%elemsNbPart(new_mail%nbElems))

        !on attrape les infos dessus
        do n=1,mail%nbElems
         if (mail%elemsNbPart(n).gt.1) then
            kk=kk+1
            new_mail%refElems(kk)=mail%refElems(kk)
            new_mail%typeElems(kk,1:2)=mail%typeElems(n,1:2)
            new_mail%elemsVertices(kk,1:3)=mail%elemsVertices(n,1:3)
            new_mail%elemsNbPart(kk)=mail%elemsNbPart(n)
         end if
      end do

       ! maintenant il nous faut les noeuds
      ! on commence par les compter
      do m=1,mail%nbNodes
         if(any(new_mail%elemsVertices== m)) then
            new_mail%nbNodes=new_mail%nbNodes+1
         end if
      end do

      allocate(new_mail%coords(new_mail%nbNodes,3))
      allocate(new_mail%refNodes(new_mail%nbNodes))
      allocate(new_mail%TrueNodes(new_mail%nbNodes))

      ! Et maintenant on copie les infos dessus
      do pp=1,mail%nbNodes
         if(any(new_mail%elemsVertices== pp)) then
            mm=mm+1
            new_mail%coords(mm,1:3)=mail%coords(pp,1:3)
            new_mail%refNodes(mm)=mail%refNodes(pp)
            new_mail%TrueNodes(mm)=pp
         end if
      end do

      ! En fait le new_mail%triVertices d'avant est faux, puisqu'il
      ! contient des refernces aux anciens noeuds. Il faut reparer ça.
      do ind11=1,new_mail%nbElems
         do ind22=1,new_mail%typeElems(ind11,2)
            loco = minloc(abs(new_mail%TrueNodes - new_mail%elemsVertices(ind11,ind22)), 1)
            new_mail%elemsVertices(ind11,ind22)=loco
         end do
      end do

   else
      print *, '**********************'
         print *, 'Partition numero ', myRank

      end if

      ! Test
      !do nt=1,new_mail%nbNodes
      !  write(*,*) new_mail%TrueNodes(nt)
      !end do

      ! du coup on a juste a choper les noeuds et les elements
      call getTriangles(new_mail)

    END SUBROUTINE sous_maillage

end module amsta01maillage
