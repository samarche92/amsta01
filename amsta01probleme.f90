! ----------------------------------------------------------------
! module de résolution d'un problème de laplacien par éléments finis P1
! Auteur : N. Kielbasiewicz
! -----------------------------------------------------------------
MODULE amsta01probleme
  USE amsta01maillage
  USE amsta01sparse
  TYPE probleme
    TYPE(maillage), POINTER :: mesh
    REAL(kind=8), DIMENSION(:), POINTER :: uexa, g, u, f, felim, u_poub1,u_poub2
    TYPE(matsparse) :: p_K, p_M, p_Kelim
  END TYPE
  CONTAINS
    ! construit un probleme à partir d'un maillage
    SUBROUTINE loadFromMesh(pb,msh)
      TYPE(probleme), INTENT(inout) :: pb
      TYPE(maillage), INTENT(in), TARGET :: msh
      INTEGER :: n, nt
      REAL(kind=8) :: Pi,x,y
      pb%mesh => msh

      n=pb%mesh%nbNodes
      nt=pb%mesh%nbTri

      Pi=4.d0*datan(1.d0)
      
      print *, 'yolo'

      ALLOCATE(pb%uexa(n),pb%g(n),pb%f(n),pb%felim(n),pb%u(n))
      pb%uexa=0.d0
      pb%g=0.d0
      pb%f=0.d0
      pb%felim=0.d0
      pb%u=0.d0
      CALL sparse(pb%p_K,n,n)
      CALL sparse(pb%p_M,n,n)
      CALL sparse(pb%p_Kelim,n,n)
      DO i=1,n
        ! initialisation de la solution theorique, du second membre et de la condition aux limites
        x=pb%mesh%coords(i,1)
        !y=pb%mesh%coords(1,i)
       	pb%uexa(i)=pb%mesh%coords(i,1)
        !pb%uexa(i)=dsin(2.d0*Pi*x)*dsin(2.d0*Pi*y)
        !pb%f(i)=8.d0*Pi**2*dsin(2.d0*Pi*x)*dsin(2.d0*Pi*y)
        ! g est la restriction de uexa sur le bord
        IF (pb%mesh%refNodes(i) == pb%mesh%refNodes(1) ) THEN
           pb%g(i)=pb%uexa(i)
        ENDIF
      END DO
    END SUBROUTINE loadFromMesh
    ! assemblage des matrices de rigidité et de masse, et du second membre
    SUBROUTINE assemblage(pb)
      TYPE(probleme), INTENT(inout) :: pb
      REAL(kind=8), DIMENSION(2) :: s1,s2,s3
      INTEGER, DIMENSION(3) :: s
      REAL(kind=8), DIMENSION(9) :: kel, mel
      INTEGER :: nt, i, j, k
      REAL(kind=8) :: x, y

      nt=pb%mesh%nbTri

      DO i=1,nt
        s=pb%mesh%triVertices(i,1:3)
        s1=pb%mesh%coords(s(1),1:2)
        s2=pb%mesh%coords(s(2),1:2)
        s3=pb%mesh%coords(s(3),1:2)

        kel=kelem(s1,s2,s3)
        mel=melem(s1,s2,s3)

        DO j=1,3
          DO k=1,3
            CALL addtocoeff(pb%p_K,s(j),s(k),kel(3*(j-1)+k))
            CALL addtocoeff(pb%p_M,s(j),s(k),mel(3*(j-1)+k))
          END DO
        END DO
      END DO

      CALL sort(pb%p_K)
      CALL sort(pb%p_M)

      pb%f=spmatvec(pb%p_M,pb%f)
    END SUBROUTINE assemblage
    ! pseudo-élimination des conditions essentielles
    !     pb : problème sur lequel appliquer la pseudo-élimination
    !     id : numéro du domaine de bord
    SUBROUTINE pelim(pb,id)
      TYPE(probleme), INTENT(inout) :: pb
      INTEGER, INTENT(in) :: id
      INTEGER, DIMENSION(:), POINTER :: indelim
      INTEGER :: n, nn, i, ii, j
      REAL(kind=8) :: val

      pb%felim=pb%f-spmatvec(pb%p_K,pb%g)
      pb%p_Kelim=pb%p_K

      n=pb%mesh%nbNodes
      nn=COUNT(pb%mesh%refNodes == id)
      ALLOCATE(indelim(nn))
      indelim=PACK((/ (i, i=1,n) /),pb%mesh%refNodes == id)

      DO ii=1,nn
        i=indelim(ii)
        val=coeff(pb%p_K,i,i)
        pb%felim(i)=pb%g(i)*val
        DO j=1,n
          IF (j /= i) THEN
            CALL delcoeff(pb%p_Kelim,i,j)
            CALL delcoeff(pb%p_Kelim,j,i)
          END IF
        END DO
      END DO
    END SUBROUTINE pelim
    ! calcul de la matrice de rigidité élémentaire
    FUNCTION kelem(s1,s2,s3) RESULT(kel)
      REAL(kind=8), DIMENSION(:), INTENT(in) :: s1,s2,s3
      REAL(kind=8), DIMENSION(9) :: kel
      REAL(kind=8) :: x12,x23,x31,y12,y13,y31, a

      x12=s1(1)-s2(1)
      x23=s2(1)-s3(1)
      x31=s3(1)-s1(1)
      y12=s1(2)-s2(2)
      y23=s2(2)-s3(2)
      y31=s3(2)-s1(2)
      a=2.d0*dabs(x23*y31-x31*y23)

      kel(1)=(x23*x23+y23*y23)/a
      kel(2)=(x23*x31+y23*y31)/a
      kel(3)=(x23*x12+y23*y12)/a
      kel(4)=kel(2)
      kel(5)=(x31*x31+y31*y31)/a
      kel(6)=(x31*x12+y31*y12)/a
      kel(7)=kel(3)
      kel(8)=kel(6)
      kel(9)=(x12*x12+y12*y12)/a
    END FUNCTION kelem
    ! calcul de la matrice de masse élémentaire
    FUNCTION melem(s1,s2,s3) RESULT(mel)
      REAL(kind=8), DIMENSION(:), INTENT(in) :: s1,s2,s3
      REAL(kind=8), DIMENSION(9) :: mel
      REAL(kind=8) :: x12,x23,x31,y12,y13,y31, a1, a2

      ! x12=s1(1)-s2(1)
      x23=s2(1)-s3(1)
      x31=s3(1)-s1(1)
      ! y12=s1(2)-s2(2)
      y23=s2(2)-s3(2)
      y31=s3(2)-s1(2)
      a1=dabs(x23*y31-x31*y23)/12.d0
      a2=a1/2.d0

      mel(1)=a1
      mel(2)=a2
      mel(3)=a2
      mel(4)=a2
      mel(5)=a1
      mel(6)=a2
      mel(7)=a2
      mel(8)=a2
      mel(9)=a1
    END FUNCTION melem
    ! calcul de la solution du problème par factorisation LU
    SUBROUTINE solveLU(pb)
      TYPE(probleme), INTENT(inout) :: pb
      TYPE(matsparse) :: L, U
      CALL lufact(pb%p_Kelim,L,U)
      CALL lusolve(L,U,pb%felim, pb%u)
    END SUBROUTINE solveLU

    ! calcul de la solution du problème avec Jacobi
    SUBROUTINE Jacobi(pb)
      TYPE(probleme),INTENT(inout) :: pb
      TYPE(matsparse) :: M,N,Jac
      CALL MatJacobi(pb%p_Kelim, M,N,Jac)
      CALL Jacsolve(M,N,pb%u,pb%felim)
    END SUBROUTINE Jacobi

    ! calcul de la solution avec Gauss-Seidel
    SUBROUTINE Gausseidel(pb)
      TYPE(probleme), INTENT(inout)::pb
      TYPE(matsparse) :: M,N
      CALL MatGauss(pb%p_Kelim,M,N)
      CALL Gaussolve(M,N,pb%u,pb%felim)
    END SUBROUTINE Gausseidel

    ! On va remettre les solutions sur les points qu'elles méritent
    subroutine backHome(pb,new_pb)
      type(probleme), intent(inout) :: pb
      type(probleme), intent(in) :: new_pb
      integer :: i

      !allocate(pb%u(mail_initial%nbNodes))

      do i=1,new_pb%mesh%nbNodes
         pb%u(new_pb%mesh%TrueNodes(i))=pb%u(new_pb%mesh%TrueNodes(i))+new_pb%u(i)
      end do

      print *, "cest bon"
    end subroutine backHome

    ! export de la solution au format vtu pour Paraview
    !     mesh : mailllage
    !     sol : vecteur solution
    !     solexa : vecteur solution exacte
    !     fname : nom du fichier de sortie (optionel)
    !             le nom doit contenir l'extension .vtu
    SUBROUTINE saveToVtu(mesh, sol, solexa, fname)
      TYPE(maillage), INTENT(in) :: mesh
      REAL(kind=8), DIMENSION(mesh%nbNodes), INTENT(in) :: sol, solexa
      CHARACTER(len=*), INTENT(in), OPTIONAL :: fname
      CHARACTER(len=300) :: filename, n1, n2, tmp
      INTEGER :: i

      filename="sol.vtu"
      IF (PRESENT(fname)) THEN
        filename=fname
      END IF

      OPEN(unit=19, file=filename, form='formatted', status='unknown')
      WRITE(19,*) '<VTKFile type="UnstructuredGrid" version="0.1"  byte_order="LittleEndian">'
      WRITE(19,*) '<UnstructuredGrid>'
      n1=computeAttributeFormat("NumberOfPoints",mesh%nbNodes)
      n2=computeAttributeFormat("NumberOfCells", mesh%nbTri)
      WRITE(19,*) '<Piece '//TRIM(ADJUSTL(n1))//' '//TRIM(ADJUSTL(n2))//'>'
      WRITE(19,*) '<PointData>'
      WRITE(19,*) '<DataArray type="Float64" Name="u" format="ascii">'
      DO i=1, mesh%nbNodes
        WRITE(19,*) sol(i)
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '<DataArray type="Float64" Name="uexa" format="ascii">'
      DO i=1, mesh%nbNodes
        WRITE(19,*) solexa(i)
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '<DataArray type="Float64" Name="erreur" format="ascii">'
      DO i=1, mesh%nbNodes
        WRITE(19,*) sol(i)-solexa(i)
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '</PointData>'
      WRITE(19,*) '<Points>'
      WRITE(19,*) '<DataArray type="Float64" Name="Nodes" NumberOfComponents="3" format="ascii">'
      DO i=1, mesh%nbNodes
        WRITE(19,*) mesh%coords(i,:)
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '</Points>'
      WRITE(19,*) '<Cells>'
      WRITE(19,*) '<DataArray type="Int32" Name="connectivity" format="ascii">'
      DO i=1, mesh%nbTri
        WRITE(19,*) mesh%triVertices(i,:)-1
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '<DataArray type="Int32" Name="offsets" format="ascii">'
      DO i=1, mesh%nbTri
        WRITE(19,*) 3*i
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '<DataArray type="UInt8" Name="types" format="ascii">'
      DO i=1, mesh%nbTri
        WRITE(19,*) 5
      END DO
      WRITE(19,*) '</DataArray>'
      WRITE(19,*) '</Cells>'
      WRITE(19,*) '</Piece>'
      WRITE(19,*) '</UnstructuredGrid>'
      WRITE(19,*) '</VTKFile>'
      CLOSE(19)
    END SUBROUTINE saveToVtu
    ! fonction qui permet de construire une chaîne de type attribut
    ! utilisée dans saveToVtu
    FUNCTION computeAttributeFormat(s,i) RESULT(n)
      CHARACTER(len=*), INTENT(in) :: s
      INTEGER, INTENT(in) :: i
      CHARACTER(len=100) :: n, istr
      WRITE(istr,*) i
      n=s//'="'//TRIM(ADJUSTL(istr))//'"'
    END FUNCTION computeAttributeFormat
END MODULE amsta01probleme
